This was project initiated by Tyler Jordan, Justin Campbell, and Steven Cutlip 
and funded by The University of New Mexico's Electrical and Computer Engineering 
department's Innovation Plaza program.

The LabVIEW real-time code is based off of the MATLAB code included with the MIT
opencourseware project.

https://ocw.mit.edu/resources/res-ll-003-build-a-small-radar-system-capable-of-sensing-range-doppler-and-synthetic-aperture-radar-imaging-january-iap-2011/index.htm